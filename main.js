let list = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
function createList (array) {
    let ul = document.createElement('ul');
    array.map( (elem) => ul.innerHTML += `<li>${elem}</li>`);
    document.body.append(ul);
}

createList(list);
